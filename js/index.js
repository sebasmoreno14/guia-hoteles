        $( document ).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        $('.carousel').carousel({
          interval: 2000
        });

        $('#contacto').on('show.bs.modal', function(e){
            console.log('El modal se está mostrando');

            $('.btn-contacto').removeClass('btn-outline-success');
            $('.btn-contacto').addClass('btn-primary');
            $('.btn-contacto').prop('disabled', true);
        });

        $('#contacto').on('shown.bs.modal', function(e){
            console.log('El modal se mostró');
        });

        $('#contacto').on('hide.bs.modal', function(e){
            console.log('El modal se está oculantado');

        });

        $('#contacto').on('hidden.bs.modal', function(e){
            console.log('El modal se ocultó');
            $('.btn-contacto').removeClass('btn-primary');
            $('.btn-contacto').addClass('btn-outline-success');
            $('.btn-contacto').prop('disabled', false);
        });

    });